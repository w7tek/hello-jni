Ohhai, JNI!
-----------

This is a demonstration of how to code, compile, link, and run a Java application that uses native code to implement
some features. In this case, the static main method is implemented as a native method (demonstrating how to implement
and call code in C++ from Java), and that native code interacts with the Java runtime to call methods on Java objects.

