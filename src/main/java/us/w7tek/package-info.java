package us.w7tek;

final class JniToy {

    static {
        System.loadLibrary("JniToy");
    }

    public static native void main(String[] args);

    static void sayHello() {
        System.out.printf("ohhai on System.out from Java method sayHello!\n");
    }
}

