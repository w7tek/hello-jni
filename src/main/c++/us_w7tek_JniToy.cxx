#include "us_w7tek_JniToy.h"

// #define USE_COUT 1

#if defined(USE_COUT)
#include <iostream>
#endif

extern "C" {

JNIEXPORT jint JNICALL
JNI_OnLoad(JavaVM *vm, void *reserved) {
#if defined(USE_COUT)
  std::cout << "hai on std::cout from onLoad function " << __FUNCTION__ << "!" << std::endl;
#endif

  return JNI_VERSION_1_8;
}

/*
  This example demonstrates that even a `main` method may be native, and how to do that.
  It also demonstrates calling static methods on Java classes, and instance methods on Java objects.
*/
JNIEXPORT void JNICALL Java_us_w7tek_JniToy_main
  (JNIEnv *jEnv, jclass j_us_w7tek_JniToy, jobjectArray args) {

#if defined(USE_COUT)
  std::cout << "hai on std::cout from native method " << __FUNCTION__ << "!" << std::endl;
#endif

  jclass j_java_lang_Object = jEnv->FindClass("java/lang/Object");

  jclass j_java_lang_System = jEnv->FindClass("java/lang/System");
  jfieldID j_java_lang_System_outID = jEnv->GetStaticFieldID(j_java_lang_System, "out", "Ljava/io/PrintStream;");
  jobject j_java_lang_System_out = jEnv->GetStaticObjectField(j_java_lang_System, j_java_lang_System_outID);

  jclass j_java_io_PrintStream = jEnv->FindClass("java/io/PrintStream");

  jmethodID j_java_io_PrintStream_println = jEnv->GetMethodID(j_java_io_PrintStream, "printf", "(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;");

  jstring message = jEnv->NewStringUTF("hai on System.out, via jni from native method %1$s!\n");
  jstring functionName = jEnv->NewStringUTF(__FUNCTION__);
  jobjectArray formatArgs = jEnv->NewObjectArray(1, j_java_lang_Object, functionName);

  jEnv->CallObjectMethod(j_java_lang_System_out, j_java_io_PrintStream_println, message, formatArgs);
  jEnv->ExceptionDescribe();

  jmethodID j_us_w7tek_JniToy_sayHello = jEnv->GetStaticMethodID(j_us_w7tek_JniToy, "sayHello", "()V");
  jEnv->CallStaticVoidMethod(j_us_w7tek_JniToy, j_us_w7tek_JniToy_sayHello);
}

}
