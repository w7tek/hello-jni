# This makefile is only known to work on Mac OS X version 10.10. YMMV.

default: java target/libJniToy.dylib
	java -cp target/classes -Djava.library.path=target us.w7tek.JniToy
	

java: src/main/java/**/*
	mvn compile

target/libJniToy.dylib: src/main/c++/*
	cc --shared -I ${JAVA_HOME}/include -I ${JAVA_HOME}/include/darwin -lstdc++ -o target/libJniToy.dylib src/main/c++/us_w7tek_JniToy.cxx

clean:
	mvn clean
